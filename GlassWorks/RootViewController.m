//
//  RootViewController.m
//  GlassWorks
//
//  Created by Manny on 5/31/14.
//  Copyright (c) 2014 Manpreet. All rights reserved.
//

#import "RootViewController.h"
#import "ViewController.h"

@interface RootViewController () <UITableViewDelegate, UITableViewDataSource>

@property IBOutlet UITableView *tableView;

@end

@implementation RootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	self.title = @"AR Indy";
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];

	[self.navigationController.navigationBar setTranslucent:NO];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 70.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *cellIdentifier = @"MenuCell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
	}

	if (indexPath.row == 0) {
		cell.textLabel.text = @"Potholes";
	}
	else if (indexPath.row == 1) {
		cell.textLabel.text = @"Bus Stops";
	}
	else if (indexPath.row == 2) {
		cell.textLabel.text = @"Rain";
	}
	else if (indexPath.row == 3) {
		cell.textLabel.text = @"Donation Centers";
	}
	else if (indexPath.row == 4) {
		cell.textLabel.text = @"Hospitals";
	}

	cell.textLabel.textAlignment = NSTextAlignmentCenter;
	cell.textLabel.font = [UIFont systemFontOfSize:30];

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	ViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];

	if (indexPath.row == 0) {
		viewController.type = 0;
	}
	else if (indexPath.row == 1) {
		viewController.type = 1;
	}
	else if (indexPath.row == 2) {
		viewController.type = 2;
	}
	else if (indexPath.row == 3) {
		viewController.type = 0;
	}
	else if (indexPath.row == 4) {
		viewController.type = 0;
	}

	[self.navigationController pushViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

/*
   #pragma mark - Navigation

   // In a storyboard-based application, you will often want to do a little preparation before navigation
   - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
   {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
   }
 */

@end
