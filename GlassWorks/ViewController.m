//
//  ViewController.m
//  GlassWorks
//
//  Created by Manny on 5/31/14.
//  Copyright (c) 2014 Manpreet. All rights reserved.
//

#import "ViewController.h"

#include <stdlib.h>


#define NUMBER_OF_POINTS    20

@interface ViewController ()
{
    PRARManager *manager;
}
@end

@implementation ViewController

- (id)initWithCoder:(NSCoder*)aDecoder
{
    if(self = [super initWithCoder:aDecoder]) {
        // Do something
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    manager = [[PRARManager alloc] initWithScreenSize:self.view.frame.size withRadar:YES andDelegate:self];
    //[PRARManager sharedManagerWithRadarAndSize:self.view.frame.size andDelegate:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setTranslucent:YES];
    // Initialize your current location as 0,0 (since it works with our randomly generated locations)
    CLLocationCoordinate2D locationCoordinates = CLLocationCoordinate2DMake(0, 0);
    
    [manager startARWithData:[self getData]
                                     forLocation:locationCoordinates];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    NSLog(@"viewWillDisappear");
    
    [manager stopAR];
}

- (void)alert:(NSString*)title withDetails:(NSString*)details {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:details
                                                   delegate:nil
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
}

#pragma mark - Dummy AR Data

-(NSArray*)getData
{
//    NSMutableArray *points = [NSMutableArray arrayWithCapacity:NUMBER_OF_POINTS];
//    
//    srand48(time(0));
//    for (int i=0; i<NUMBER_OF_POINTS; i++)
//    {
//        CLLocationCoordinate2D pointCoordinates = [self getRandomLocation];
//        NSDictionary *point = [self createPointWithId:i at:pointCoordinates];
//        [points addObject:point];
//    }
//    
//    return [NSArray arrayWithArray:points];
    
    NSMutableArray *points = [[NSMutableArray alloc] init];
    
    if (self.type == 0) {
        
        NSString* path = [[NSBundle mainBundle] pathForResource:@"potholes"
                                                         ofType:@"txt"];
        NSString* content = [NSString stringWithContentsOfFile:path
                                                      encoding:NSUTF8StringEncoding
                                                         error:NULL];
        
        NSArray* rows = [content componentsSeparatedByString:@"\n"];
        [rows enumerateObjectsUsingBlock:^(NSString *row, NSUInteger idx, BOOL *stop) {
            NSArray* columns = [row componentsSeparatedByString:@","];
            NSLog(@"%@", columns);
            
            NSDictionary *point = @{
                                    @"id" : @(idx),
                                    @"title" : [columns objectAtIndex:0],
                                    @"lon" : [NSString stringWithFormat:@"%@", [columns objectAtIndex:1]],
                                    @"lat" : [NSString stringWithFormat:@"%@", [columns objectAtIndex:2]]
                                    };
            [points addObject:point];
        }];
        
    } else if (self.type == 1) {
        
        NSString* path = [[NSBundle mainBundle] pathForResource:@"stops"
                                                         ofType:@"txt"];
        NSString* content = [NSString stringWithContentsOfFile:path
                                                      encoding:NSUTF8StringEncoding
                                                         error:NULL];
        
        NSArray* rows = [content componentsSeparatedByString:@"\n"];
        [rows enumerateObjectsUsingBlock:^(NSString *row, NSUInteger idx, BOOL *stop) {
            NSArray* columns = [row componentsSeparatedByString:@","];
            NSLog(@"%@", columns);
            
            NSDictionary *point = @{
                                    @"id" : @(idx),
                                    @"title" : [columns objectAtIndex:1],
                                    @"lon" : [NSString stringWithFormat:@"%@", [columns objectAtIndex:2]],
                                    @"lat" : [NSString stringWithFormat:@"%@", [columns objectAtIndex:3]]
                                    };
            [points addObject:point];
        }];
        
    } else if (self.type == 2) {
        
        NSString* path = [[NSBundle mainBundle] pathForResource:@"rain"
                                                         ofType:@"txt"];
        NSString* content = [NSString stringWithContentsOfFile:path
                                                      encoding:NSUTF8StringEncoding
                                                         error:NULL];
        
        NSArray* rows = [content componentsSeparatedByString:@"\n"];
        [rows enumerateObjectsUsingBlock:^(NSString *row, NSUInteger idx, BOOL *stop) {
            NSArray* columns = [row componentsSeparatedByString:@","];
            NSLog(@"%@", columns);
            
            NSDictionary *point = @{
                                    @"id" : @(idx),
                                    @"title" : [columns objectAtIndex:0],
                                    @"lon" : [NSString stringWithFormat:@"%@", [columns objectAtIndex:1]],
                                    @"lat" : [NSString stringWithFormat:@"%@", [columns objectAtIndex:2]]
                                    };
            [points addObject:point];
        }];
        
    }
    
    return [NSArray arrayWithArray:points];
}

// Returns a random location
-(CLLocationCoordinate2D)getRandomLocation
{
    double latRand = drand48() * 90.0;
    double lonRand = drand48() * 180.0;
    double latSign = drand48();
    double lonSign = drand48();
    
    CLLocationCoordinate2D locCoordinates = CLLocationCoordinate2DMake(39.869563,
                                                                       -86.140848);
    return locCoordinates;
}

// Creates the Data for an AR Object at a given location
-(NSDictionary*)createPointWithId:(int)the_id at:(CLLocationCoordinate2D)locCoordinates
{
    NSDictionary *point = @{
                            @"id" : @(the_id),
                            @"title" : [NSString stringWithFormat:@"Place Num %d", the_id],
                            @"lon" : @(locCoordinates.longitude),
                            @"lat" : @(locCoordinates.latitude)
                            };
    return point;
}


#pragma mark - PRARManager Delegate

-(void)prarDidSetupAR:(UIView *)arView withCameraLayer:(AVCaptureVideoPreviewLayer *)cameraLayer andRadarView:(UIView *)radar
{
    NSLog(@"Finished displaying ARObjects");
    
    [self.view.layer addSublayer:cameraLayer];
    [self.view addSubview:arView];
    
    [self.view bringSubviewToFront:[self.view viewWithTag:AR_VIEW_TAG]];
    
    [self.view addSubview:radar];
    
    [loadingV setHidden:YES];
}

-(void)prarUpdateFrame:(CGRect)arViewFrame
{
    [[self.view viewWithTag:AR_VIEW_TAG] setFrame:arViewFrame];
}

-(void)prarGotProblem:(NSString *)problemTitle withDetails:(NSString *)problemDetails
{
    [loadingV setHidden:YES];
    [self alert:problemTitle withDetails:problemDetails];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
