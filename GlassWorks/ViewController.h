//
//  ViewController.h
//  GlassWorks
//
//  Created by Manny on 5/31/14.
//  Copyright (c) 2014 Manpreet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRARManager.h"

@interface ViewController : UIViewController <PRARManagerDelegate>
{
    IBOutlet UIView *loadingV;
}

@property (nonatomic) int type;

@end
