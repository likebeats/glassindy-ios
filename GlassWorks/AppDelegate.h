//
//  AppDelegate.h
//  GlassWorks
//
//  Created by Manny on 5/31/14.
//  Copyright (c) 2014 Manpreet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
